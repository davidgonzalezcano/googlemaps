package cat.itb.googlemaps.models;

import java.io.Serializable;

public class Marcadors implements Serializable {
    private String id;
    private String name;
    private String category;
    private String image;
    private String latitud;
    private String longitud;

    public Marcadors() {
    }

    public Marcadors(String id, String name, String category, String image, String latitud, String longitud) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.image = image;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public String getImage() {
        return image;
    }

    public String getLatitud() {
        return latitud;
    }

    public String getLongitud() {
        return longitud;
    }
}