package cat.itb.googlemaps.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import cat.itb.googlemaps.R;
import cat.itb.googlemaps.models.Marcadors;
import id.zelory.compressor.Compressor;

import static android.app.Activity.RESULT_OK;

public class Fragment_New_Marcador extends Fragment {
    private ProgressBar cargando;
    File url;
    TextInputLayout outlinedTextField;
    double latitud;
    double longitud;
    EditText name_marcador;
    Button create_marcador_btn;
    Spinner category_marcador;
    TextView successfull;
    ImageView picture_ImageView;
    TextView spinner_error_text;
    TextView image_error_text;
    boolean crop_image;
    Bitmap thumb_bitmap;
    byte[] thumb_byte;
    DatabaseReference marcadorRef;
    StorageReference storageReference;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle= getArguments();
        assert bundle != null;
        latitud = bundle.getDouble("latitude");
        longitud = bundle.getDouble("longitude");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_marcador, container, false);

        name_marcador = view.findViewById(R.id.name_marcador_editText);
        create_marcador_btn = view.findViewById(R.id.create_marcador_btn);
        create_marcador_btn.setVisibility(View.VISIBLE);
        category_marcador = view.findViewById(R.id.category_marcador_spinner);
        picture_ImageView = view.findViewById(R.id.picture_ImageView);
        spinner_error_text = view.findViewById(R.id.spinner_error_text);
        image_error_text = view.findViewById(R.id.image_error_text);
        outlinedTextField = view.findViewById(R.id.outlinedTextField);
        successfull = view.findViewById(R.id.successfull);
        cargando = view.findViewById(R.id.progressBar);
        crop_image = false;
        marcadorRef = FirebaseDatabase.getInstance().getReference().child("mapas");
        storageReference = FirebaseStorage.getInstance().getReference().child("img_comprimidas");

        picture_ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(getContext(), Fragment_New_Marcador.this);
            }
        });

        create_marcador_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createMarcador();
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri= CropImage.getPickImageResultUri(getContext(), data);
            recortarImagen(imageUri);
        }

        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                url = new File(resultUri.getPath());
                Picasso.with(getContext()).load(url).into(picture_ImageView);
            }
        }
    }

    public void recortarImagen(Uri imageUri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
                .setRequestedSize(640, 480)
                .setAspectRatio(2, 1).start(getContext(), Fragment_New_Marcador.this);
        crop_image = true;

    }

    public void comprimirImagen() {
        try {
            thumb_bitmap = new Compressor(getContext())
                    .setMaxHeight(125)
                    .setMaxWidth(125)
                    .setQuality(50)
                    .compressToBitmap(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        thumb_byte = byteArrayOutputStream.toByteArray();
    }

    public void createMarcador() {
        boolean validation_ok = true;

        if (name_marcador.getText().toString().length() <= 0) {
            outlinedTextField.setError("El nom es obligatori");
            validation_ok = false;
        } else {
            outlinedTextField.setError("");
        }

        if(category_marcador.getSelectedItem().toString().equals("(Selecciona)")) {
            spinner_error_text.setVisibility(View.VISIBLE);
            validation_ok = false;
        }else {
            spinner_error_text.setVisibility(View.INVISIBLE);
        }
        if (!crop_image) {
            image_error_text.setVisibility(View.VISIBLE);
            validation_ok = false;
        }
        else {
            image_error_text.setVisibility(View.INVISIBLE);
        }

        if(validation_ok) {
            pujarMarcador();
        }
    }

    public void pujarMarcador() {
        comprimirImagen();
        cargando.setVisibility(View.VISIBLE);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timestamp = sdf.format(new Date());
        String namePicture = timestamp + ".jpg";

        StorageReference ref = storageReference.child(namePicture);
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setCustomMetadata("latitude", "" + latitud)
                .setCustomMetadata("longitude", "" + longitud)
                .build();
        UploadTask uploadTask = ref.putBytes(thumb_byte, metadata);
        Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if(!task.isSuccessful()) {
                    throw Objects.requireNonNull(task.getException());
                }
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                Uri donwloadUri = task.getResult();
                String key = marcadorRef.push().getKey();
                Marcadors marcador = new Marcadors(
                        key,
                        name_marcador.getText().toString(),
                        category_marcador.getSelectedItem().toString(),
                        donwloadUri.toString(),
                        "" + latitud,
                        "" + longitud);
                marcador.setId(key);
                marcadorRef.child(key).setValue(marcador);
                cargando.setVisibility(View.INVISIBLE);
                create_marcador_btn.setVisibility(View.INVISIBLE);
                successfull.setVisibility(View.VISIBLE);
            }
        });
    }
}
