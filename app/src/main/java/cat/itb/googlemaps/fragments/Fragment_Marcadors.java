package cat.itb.googlemaps.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import cat.itb.googlemaps.R;
import cat.itb.googlemaps.adapters.Marcador_Adapter;
import cat.itb.googlemaps.db.DataBaseHelper;
import cat.itb.googlemaps.models.Marcadors;


public class Fragment_Marcadors extends Fragment {
    public static DataBaseHelper dataBaseHelper;
    RecyclerView recyclerView;
    Marcador_Adapter marcador_adapter;
    List<Marcadors> marcadors = new ArrayList<>();

    public Fragment_Marcadors() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view, container, false);

        FirebaseDatabase firebaseDatabase;
        firebaseDatabase = FirebaseDatabase.getInstance();

        dataBaseHelper = new DataBaseHelper("mapas", getContext());

        marcador_adapter = new Marcador_Adapter(dataBaseHelper.firebaseRecyclerOptions, getContext());
        marcador_adapter.setContext(marcador_adapter.getContext());

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(marcador_adapter);

        DatabaseReference myRef;
        myRef = firebaseDatabase.getReference("mapas");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot s : dataSnapshot.getChildren()){
                    Marcadors marcador = s.getValue(Marcadors.class);
                    marcadors.add(marcador);
                }

                new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        String id = marcadors.get(viewHolder.getAdapterPosition()).getId();

                        myRef.child(id).removeValue();

                        Toast.makeText(getContext(), "Eliminado", Toast.LENGTH_SHORT).show();

                        marcador_adapter.notifyDataSetChanged();
                    }
                }).attachToRecyclerView(recyclerView);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        marcador_adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        marcador_adapter.stopListening();
    }
}