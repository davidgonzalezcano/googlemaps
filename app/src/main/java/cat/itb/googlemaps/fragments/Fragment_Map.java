package cat.itb.googlemaps.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import cat.itb.googlemaps.permissions.Permissions;
import cat.itb.googlemaps.R;
import cat.itb.googlemaps.models.Marcadors;

public class Fragment_Map extends Fragment implements OnMapReadyCallback{
    View rootView;
    MapView mapView;
    GoogleMap googleMap;
    FirebaseDatabase firebaseDatabase;
    Permissions permissions = new Permissions();
    DatabaseReference myRef;
    List<Marcadors> marcadors;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_map, container, false);
        firebaseDatabase = FirebaseDatabase.getInstance();
        myRef = firebaseDatabase.getReference("mapas");
        marcadors = new ArrayList<>();
        return  rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = rootView.findViewById(R.id.googleMap);

        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.setMinZoomPreference(15);
        this.googleMap.setMaxZoomPreference(18);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Bundle bundle = getArguments();
                if (bundle != null) {
                    double latitud = Double.parseDouble(bundle.getString("latitude"));
                    double longitud = Double.parseDouble(bundle.getString("longitude"));
                    String name = bundle.getString("name");
                    String category = bundle.getString("category");

                    LatLng posicio =  new LatLng(latitud, longitud);

                    MarkerOptions marker = new MarkerOptions();
                    marker.position(posicio);
                    marker.title(name);
                    marker.snippet(category);
                    marker.draggable(true);

                    googleMap.getUiSettings().setZoomControlsEnabled(true);
                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    googleMap.addMarker(marker);

                    permissions.enableMyLocation(googleMap, getContext(), getActivity());

                    ubicarMapa(latitud, longitud);

                } else {
                    for (DataSnapshot s : dataSnapshot.getChildren()){
                        Marcadors marcador = s.getValue(Marcadors.class);
                        marcadors.add(marcador);

                        for (int i = 0; i < marcadors.size(); i++) {
                            double latitud = Double.parseDouble(marcador.getLatitud());
                            double longitud = Double.parseDouble(marcador.getLongitud());

                            LatLng posicio =  new LatLng(latitud, longitud);

                            MarkerOptions marker = new MarkerOptions();
                            marker.position(posicio);
                            marker.title(marcador.getName());
                            marker.snippet(marcador.getCategory());
                            marker.draggable(true);

                            googleMap.getUiSettings().setZoomControlsEnabled(true);
                            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                            googleMap.addMarker(marker);

                            permissions.enableMyLocation(googleMap, getContext(), getActivity());
                        }
                    }
                    double latitudInicial = 41.45295666889963;
                    double longitudInicial = 2.1864908561110497;
                    ubicarMapa(latitudInicial, longitudInicial);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

        this.googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                Bundle bundle = new Bundle();
                bundle.putDouble("latitude", latLng.latitude);
                bundle.putDouble("longitude", latLng.longitude);

                Fragment_New_Marcador fragment_new_marcador = new Fragment_New_Marcador();
                fragment_new_marcador.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.fragment_layout, fragment_new_marcador).commit();
            }
        });
    }

    public void ubicarMapa(double latitud, double longitud) {
        LatLng itb =  new LatLng(latitud,longitud);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(itb)
                .zoom(15)
                .bearing(0f)
                .tilt(30)
                .build();
        this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}