package cat.itb.googlemaps.db;

import android.content.Context;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import cat.itb.googlemaps.models.Marcadors;

public class DataBaseHelper {
    public static FirebaseDatabase firebaseDatabase;
    public static DatabaseReference databaseReference;
    public static StorageReference storageReference;
    public static FirebaseRecyclerOptions<Marcadors> firebaseRecyclerOptions;
    Context context;

    public DataBaseHelper(String nameDatabase, Context context) {
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(nameDatabase);
        firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<Marcadors>()
                .setQuery(databaseReference, Marcadors.class).build();
        storageReference = FirebaseStorage.getInstance().getReference().child("img_comprimidas");
        this.context = context;
    }
}
