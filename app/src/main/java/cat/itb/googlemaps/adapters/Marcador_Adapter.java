package cat.itb.googlemaps.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.squareup.picasso.Picasso;
import cat.itb.googlemaps.R;
import cat.itb.googlemaps.activities.MainActivity;
import cat.itb.googlemaps.fragments.Fragment_Map;
import cat.itb.googlemaps.models.Marcadors;



public class Marcador_Adapter extends FirebaseRecyclerAdapter<Marcadors, Marcador_Adapter.MarcadorHolder> {

    public Context context;
    public View view;

    public Marcador_Adapter(@NonNull FirebaseRecyclerOptions<Marcadors> options, Context context) {
        super(options);
        this.context = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull MarcadorHolder holder, int position, @NonNull Marcadors model) {
        holder.bind(model);
    }

    @NonNull
    @Override
    public MarcadorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        this.view = view;
        return new MarcadorHolder(view);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public class MarcadorHolder extends RecyclerView.ViewHolder{
        ImageView marcador_image;
        TextView marcador_name;
        TextView marcador_category;

        public MarcadorHolder(@NonNull View itemView) {
            super(itemView);
            marcador_image = itemView.findViewById(R.id.marcador_image);
            marcador_name = itemView.findViewById(R.id.marcador_name);
            marcador_category = itemView.findViewById(R.id.marcador_category);
        }

        public void bind(Marcadors model) {
            try {
                Picasso.with(marcador_image.getContext()).load(model.getImage()).into(marcador_image);
            } catch (Exception e) {
            }
            marcador_name.setText(model.getName());
            marcador_category.setText(model.getCategory());

            itemView.setOnClickListener(v -> {
                MainActivity mainActivity = (MainActivity)context;

                Bundle bundle = new Bundle();
                bundle.putString("latitude", model.getLatitud());
                bundle.putString("longitude", model.getLongitud());
                bundle.putString("name", model.getName());
                bundle.putString("category", model.getCategory());
                bundle.putString("image", model.getImage());

                Fragment_Map mapsFragment = new Fragment_Map();
                mapsFragment.setArguments(bundle);

                mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_layout, mapsFragment).commit();
            });
        }
    }
}
