package cat.itb.googlemaps.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.view.MenuItem;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import cat.itb.googlemaps.R;
import cat.itb.googlemaps.fragments.Fragment_Main;
import cat.itb.googlemaps.fragments.Fragment_Map;
import cat.itb.googlemaps.fragments.Fragment_Marcadors;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CrearMenu(0);

        // Setting Home Fragment as main fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_layout, new Fragment_Main()).commit();
    }

    // Listener Nav Bar
    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new
            BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;
                    switch (item.getItemId()) {
                        case R.id.mapa_item:
                            CrearMenu(1);
                            selectedFragment = new Fragment_Map();

                            break;
                        case R.id.marcador_item:
                            selectedFragment = new Fragment_Marcadors();
                            break;
                    }

                    // Begin Transaction
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_layout
                            ,selectedFragment).commit();
                    return true;
                }
    };

    public void CrearMenu(int numero) {
        BottomNavigationView btnNav = findViewById(R.id.bottom_navigation_view);
        if(numero == 0) {
            btnNav.getMenu().getItem(0).setCheckable(false);
        }
        else {
            btnNav.getMenu().getItem(0).setCheckable(true);
        }
        btnNav.setOnNavigationItemSelectedListener(navListener);
    }
}